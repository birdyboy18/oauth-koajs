module.exports = {
    instagram: {
        client_id: `CLIENT_ID`,
        client_secret: `CLIENT_SECRET`,
        callback_url: `callback url registered when making the app`
    }
}
