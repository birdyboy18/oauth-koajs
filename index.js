const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const session = require('koa-session');
const router = require('./routes');
const SessionService = require('./services').SessionService;
const app = new Koa();

app.keys = ['secret key stuffs'];
const CONFIG = {
  key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
  //maxAge: 86400000, /** (number) maxAge in ms (default is 1 days) */
  maxAge: 10000,
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  store: new SessionService()
};
app.use(bodyParser());
app.use(session(CONFIG,app));
app.use(router.routes());

app.listen(5050, () => {
	console.log(`App is listening on port http://localhost:5050`);
});
