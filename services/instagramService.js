const config = require('../config');
const r = require('request');

class InstagramService {
	constructor(opts) {
		this.baseUrl = `https://api.instagram.com`;
		this.client_id = opts.client_id;
		this.client_secret = opts.client_secret;
		this.callback_url = opts.callback_url;
	}

	authorise(code) {
		return new Promise((resolve, reject) => {
			const url = this.baseUrl + `/oauth/access_token`;
			r.post(url, { form: {
					client_id: this.client_id,
					client_secret: this.client_secret,
					grant_type: `authorization_code`,
					redirect_uri: this.callback_url,
					code
				}}, (err, res, body) => {
					if (err) { reject(err); }
					if (res.statusCode == 200) {
						resolve(body);
					} else {
						reject(body);
					}
				});
		});
	}
}

const instance = new InstagramService({
	client_id: config.instagram.client_id,
	client_secret: config.instagram.client_secret,
	callback_url: config.instagram.callback_url
});

module.exports = instance;
