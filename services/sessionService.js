const Session = require('../models').Session;

class SessionService {
	constructor() {

	}

	get instance() {
		return Session;
	}

	get(key) {
		return new Promise((resolve, reject) => {
			Session.forge().query({ where: { id: key }}).fetch().then(session => {
				if (session) {
					resolve(JSON.parse(session.get('data')));
				} else {
					resolve({});
				}
			});
		});
	}

	set(key, sess, maxAge) {
		return new Promise((resolve, reject) => {
			Session.forge().query({ where: { id: key }}).fetch().then(session => {
				if (session) {
					console.log(`session exists, update it`);
					let promise = session.save({
						id: key,
						data: JSON.stringify(sess),
						expiresAt: sess._expire
					});
					resolve(promise);
				} else {
					console.log(`session doesn't exists so make one`);
					let promise = Session.forge().save({
						id: key,
						data: JSON.stringify(sess),
						expiresAt: sess._expire
					});
					resolve(promise);
				}
			});
		});
	}

	destroy(key) {
		return new Promise((resolve, reject) => {
			Session.forge().query({ where: { id: key }}).fetch().then(session => {
				resolve(session.destroy());
			});
		});
	}
}

module.exports = SessionService;
