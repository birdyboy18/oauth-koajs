const db = require('../database');
const bcrypt = require('bcrypt');

const User = db.Model.extend({
    tableName: 'users',
	hasTimestamps: true,

    constructor: function() {
        db.Model.apply(this, arguments);
		this.on('saving', async (model, attrs, opts) => {
			//check if an attrs is password
			if (attrs.password) {
				const hash = await this.hashPassword(attrs.password);
				model.set('password', hash);
			}
		});
	},

    completedSignup: function() {
		return this.get('completed_signup');
	},

	hashPassword: function(password) {
		return new Promise(( resolve, reject ) => {
			bcrypt.hash(password, 10).then(hash => {
				resolve(hash);
			});
		});
	},

	verifyPassword: function(password) {
		return bcrypt.compare(password, this.get('password'));
	}
}, {
    findByUsername: function(username) {
        return new Promise((resolve, reject) => {
            this.forge().query({ where: { username }}).fetch().then(u => {
                if (u) {
                    resolve(u);
                } else {
                    reject({
                        code: 12,
                        message: `Couldn't find user with username`
                    });
                }
            }).catch(err => {
                reject(err);
            });
        });
    },

    create: function(data) {
        return new Promise((resolve, reject) => {
            this.forge().query({ where: { username: data.username }}).fetch().then(user => {
                if (user) {
                    reject({
                        code: 20,
                        message: `User already exists`,
                        username: user.toJSON().username
                    });
                } else {
                    resolve(this.forge().save(data));
                }
            });
        });
    },
});

module.exports = User;
