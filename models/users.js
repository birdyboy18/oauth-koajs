const db = require('../database');

class Users extends db.Collection {
	constructor() {
		super();
	}

	get model() {
		return 'User';
	}
}

module.exports = Users;
