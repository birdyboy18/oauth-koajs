const db = require('../database');

const Session = db.Model.extend({
	tableName: 'sessions',
	hasTimestamps: true,

	//use these like getters and setters
	getSession: function(key) {
		return new Promise((resolve, reject) => {
			this.forge().query({ where: { id: key }}).fetch().then(session => {
				if (session) {
					resolve(JSON.parse(session.get(`data`)));
				} else {
					resolve({});
					reject({
						message: `Couldn't find session`
					});
				}
			});
		});
	},

	setSession: function(key, sess, maxAge) {
		return new Promise((resolve, reject) => {
			this.forge().query({ where: { id: key }}).fetch().then(session => {
				if (session) {
					console.log(`session exists, update it`);
					let promise = session.save({
						id: key,
						data: JSON.stringify(sess),
						expiresAt: sess._expire
					});
					resolve(promise);
				} else {
					console.log(`session doesn't exists so make one`);
					let promise = this.forge().save({
						id: key,
						data: JSON.stringify(sess),
						expiresAt: sess._expire
					});
					resolve(promise);
				}
			});
		});
	},

	destroySession: function(key) {
		return new Promise((resolve, reject) => {
			this.forge().query({ where: { id: key }}).fetch().then(session => {
				resolve(session.destroy());
			});
		});
	}
}, {
	//Methods you can call on an instance of a model
	removeExpired: function() {
		return new Promise((resolve, reject) => {
			this.forge().query('where', 'expiresAt', '<', Date.now()).fetchAll().then(collection => {
				let promises = [];
				collection.forEach(session => {
					promises.push(session.destroy());
				});
				return Promise.all(promises);
			});
		});
	}
});

module.exports = Session;
