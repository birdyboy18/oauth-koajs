const path = require('path');
const dbConfig = require(path.join(__dirname, '../knexfile'));
const knex = require('knex')(dbConfig['development']);
const bookshelf = require('bookshelf')(knex);

module.exports = bookshelf;

knex.migrate.latest(dbConfig);
