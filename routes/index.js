const Router = require('koa-router');
const fs = require('fs');
const path = require('path');
const Services = require('../services');
const Middleware = require('../middleware');
const User = require('../models').User;

let router = new Router();

router.get('/', async (ctx, next) => {
	ctx.body = `Welcome to the root.`;
});

router.get('/signup', async (ctx, next) => {
	let stats = fs.statSync(path.join(__dirname, '../public/index.html'));
	ctx.type = `text/html`;
	ctx.body = fs.readFileSync(path.join(__dirname, '../public/index.html'), 'utf8');
});

router.get('/signup/:username', async (ctx, next) => {
    try {
        const user = await User.findByUsername(ctx.params.username);
		if (user.completedSignup() != true) {
			let stats = fs.statSync(path.join(__dirname, '../public/finishSignup.html'));
	    	ctx.type = `text/html`;
	    	ctx.body = fs.readFileSync(path.join(__dirname, '../public/finishSignup.html'), 'utf8');
		} else {
			ctx.redirect('/');
		}
    } catch(err) {
        if (err.code === 12) {
            ctx.redirect('/signup');
        } else {
            console.log(err);
            ctx.body = JSON.parse(err);
        }
    }
});

router.post('/signup/:username', async (ctx, next) => {
    try {
        let body = ctx.request.body;
        if (body.password && body.email) {
            const user = await User.findByUsername(ctx.params.username);

			if (user.completedSignup() != true) {
				user.save({
					'email': body.email,
					'password': body.password,
                    'completed_signup': true
				});
				ctx.body = 'updating';
			} else {
				ctx.status = 400;
				ctx.body = {
					code: 27,
					message: `User has already finished signup`
				}
			}
        } else {
            ctx.status = 400;
            ctx.body = {
                message: `you need to provide an email and password`
            }
        }
    } catch(err) {
		console.log(err);
        ctx.body = JSON.parse(err);
    }
});

router.get('/authorise/instagram', async (ctx, next) => {
	if (ctx.query.code) {
		let code = ctx.query.code;
		try {
			let tokenPayload = JSON.parse(await Services.InstagramService.authorise(code));
			let newUser = await User.create({
				service: `instagram`,
				service_id: tokenPayload.user.id,
				username: tokenPayload.user.username,
				name: tokenPayload.user.full_name,
				profile_picture: tokenPayload.user.profile_picture,
				access_token: tokenPayload.access_token
			});
			ctx.body = `New user created ${JSON.stringify(newUser)}`;
		} catch(err) {
			//handle some error stuff. Log to file, or re-attempt
			if (err.code === 20) { // user already exists
				ctx.redirect(`/signup/${err.username}`);
			} else {
				ctx.body = err;
			}
		}
	}
});

router.get('/user/:username', async (ctx, next) => {
    try {
        const user = await User.findByUsername(ctx.params.username);
        ctx.body = user;
    } catch(err) {
        console.log(err);
        ctx.body = err;
    }
});

router.post('/authenticate', async (ctx, next) => {
	const body = ctx.request.body;
	try {
		const user = await User.findByUsername(body.username);
		const verified = await user.verifyPassword(body.password);
		/**
		* Do some stuff, maybe provide an option?
		* sessions for browsers
		* Assign JWT for REST API Usages
		*/
		console.log(verified);
		if (verified) {
			console.log('success login');
			ctx.session.user = {
				username: user.get('username'),
				password: user.get('password')
			}
			ctx.redirect('/');
		} else {
			ctx.redirect('/login');
		}
	} catch(err) {
		console.log(err);
		//ctx.body = JSON.parse(err);
	}
})

router.get('/login', async (ctx, next) => {
	let stats = fs.statSync(path.join(__dirname, '../public/login.html'));
	ctx.type = `text/html`;
	ctx.body = fs.readFileSync(path.join(__dirname, '../public/login.html'), 'utf8');
});

router.get('/logout', async (ctx, next) => {
	ctx.session = null;
	ctx.redirect('/login');
});

router.get('/protected', Middleware.Auth.checkSession, async (ctx, next) => {
	ctx.body = `I am a protected route`;
});

module.exports = router;
