const User = require('../models/').User;

class Auth {
	constructor() {

	}

	async checkSession(ctx, next) {
		if (ctx.session.user) {
			const sesUser = ctx.session.user;

			const user = await User.findByUsername(ctx.session.user.username);
			if (user) {
				if (user.get('username') === sesUser.username && user.get('password') === sesUser.password) {
					next();
				} else {
					ctx.redirect('/login');
				}
			}
		} else {
			ctx.redirect('/login');
		}
	}
}

const instance = new Auth();

module.exports = instance;
