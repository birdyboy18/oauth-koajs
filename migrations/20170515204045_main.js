
exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTableIfNotExists('users', (table) => {
			table.increments().primary();
			table.string('service');
			table.integer('service_id');
			table.string('username');
			table.string('password');
			table.string('name');
			table.string('email');
			table.string('profile_picture');
			table.string('access_token');
			table.boolean('completed_signup').defaultTo(false);
			table.timestamps();
		}),
		knex.schema.createTableIfNotExists('sessions', table => {
			table.string('id', 40).primary();
			table.json('data');
			table.dateTime('expiresAt');
			table.timestamps();
		})
	]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('users'),
        knex.schema.dropTable('sessions')
	]);
};
